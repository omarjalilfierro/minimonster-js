'use strict';

let monster = {
    modules : {

    },
    app : {

    }
};

monster.modules.actions = (function (){

    let name, life, money, awake;

    return{
        showMe(){
            monster.modules.app.log(`Name: ${name} 
                     Life: ${life}   
                     Money: ${money} 
                     Status: ${awake}
                    `);
            monster.modules.app.displayStatus(life,money,awake);
        },
        init(parameters){
            monster.modules.app.log("The monster has borned");
            name = parameters.name;
            life = parameters.life;
            money = parameters.money;
            awake = parameters.awake;
        },
        run(){
            if (life > 0) //If life < 0
                if (awake == true) //If is asleep
                    if ((life - 1) > 0){//If has enough hp
                        life--;
                        monster.modules.app.log(`${name} looses 1hp for running`);
                        monster.modules.app.displayStatus(life,money,awake);
                    }
                    else //If has not enough hp
                        monster.modules.app.log(`${name} will die if you do that`);
                else //If is asleep
                    monster.modules.app.log(`${name} is sleeping`);
            else //If life < 0
                monster.modules.app.log(`${name} is dead`);
        },
        fight(){
            if (life > 0) //If life < 0
                if (awake == true) //If is asleep
                    if ((life - 3) > 0){//If has enough hp
                        life -= 3;
                        monster.modules.app.log(`${name} lost 3hp fighting a bear`);
                        monster.modules.app.displayStatus(life,money,awake);
                    }
                    else //If has not enough hp
                        monster.modules.app.log(`${name} will die if you do that`);
                else //If is asleep
                    monster.modules.app.log(`${name} is sleeping`);
            else //If life < 0
                monster.modules.app.log(`${name} is dead`);
        },
        work(){
            if (life > 0) //If life < 0
                if (awake == true) //If is asleep
                    if ((life - 1) > 0){//If has enough hp
                        life --;
                        money += 2;
                        monster.modules.app.log(`Here comes the money`);
                        monster.modules.app.displayStatus(life,money,awake);
                    }
                    else //If has not enough hp
                        monster.modules.app.log(`${name} will die if you do that`);
                else //If is asleep
                    monster.modules.app.log(`${name} is sleeping`);
            else //If life < 0
                monster.modules.app.log(`${name} is dead`);
        },
        eat(){
            if (life > 0) //If life < 0
                if (awake == true) //If is asleep
                    if ((money - 3) > 0){//If has enough money
                        life += 2;
                        money -= 3;
                        monster.modules.app.log('Yummm');
                        monster.modules.app.displayStatus(life,money,awake);
                    }
                    else //If has not enough money
                        monster.modules.app.log(`${name} is broke`);
                else //If is asleep
                    monster.modules.app.log(`${name} is sleeping`);
            else //If life < 0
                monster.modules.app.log(`${name} is dead`);
        },
        sleep(){
            if (awake === false){
                monster.modules.app.log(`${name} is already sleeping. He cant sleep twice`);
            }
            else if (life > 0){
                awake = false;
                monster.modules.app.log(`${name} is sleepy`);

                setTimeout(() => {
                    awake = true;
                    life++;
                    monster.modules.app.log(`${name} is full of energy!`);
                    monster.modules.app.displayStatus(life,money,awake);
                },10 * 1000);
            }
            else {
                monster.modules.app.log(`${name} is dead`);
                monster.modules.app.displayStatus(life,money,awake);
            }
        },
        interval(){
            setInterval(()=> {
                if (life != 0)
                    life--;

                let rand = Math.floor(Math.random() * (6 - 1)) + 1; //Gets a number between 1 and 5
                switch (parseInt(rand)){
                    case 1:
                        monster.modules.actions.sleep();
                        break;
                    case 2:
                        monster.modules.actions.eat();
                        break;
                    case 3:
                        monster.modules.actions.run();
                        break;
                    case 4:
                        monster.modules.actions.fight();
                        break;
                    case 5:
                        monster.modules.actions.work();
                        break;
                }
                monster.modules.app.displayStatus(life,money,awake);
            } , 12 * 1000);
        },
        reset(){
            monster.modules.actions.init({
                name  : 'Jalilo',
                life  : 100,
                money : 50,
                awake : true
            });
            let monsterBox = document.getElementById('monster'); // Changes the color of the monster depending of health
            monsterBox.classList.remove('thick','thin','medium');
            monster.modules.app.displayStatus(life,money,awake);
            monster.modules.actions.showMe();
        },
        kill(){
            life = 0;
            monster.modules.app.log(`${name} is DEAD!`);
            monster.modules.app.displayStatus(life,money,awake);
        },
        show(){
            monster.modules.app.log(`Name: ${name} 
                     Life: ${life}   
                     Money: ${money} 
                     Status: ${awake}
                    `);
        }
    }
})();

monster.modules.app = (function () {

    let actions = monster.modules.actions;

    let btnActions = ['run','fight','work','show','eat','sleep','reset','kill','show'];
    btnActions.forEach( (action) => {
       let btn = document.getElementById(action);
       btn.addEventListener('click', actions[action]);
    });

    return {
        start() {
            actions.init({
                name  : 'Jalilo',
                life  : 100,
                money : 50,
                awake : true
            });

            actions.showMe();
            actions.interval();
          // btnReset.addEventListener("click", ()=> { });
          // btnKill.addEventListener("click", ()=> { });
        },
        log(message){
            let p = document.createElement('p');
            p.innerText = message;
            document.querySelector('#actionbox').prepend(p);
        },
        displayStatus(life, money, awake){
            let monster = document.getElementById('monster'); // Changes the color of the monster depending of health
            if (life <= 5){
                monster.classList.add("red");
                monster.classList.remove("orange");
            }
            if (life > 5 && life <= 20){
                monster.classList.add("orange");
                monster.classList.remove("blue");
            }
            if (life > 20 && life < 50){
                monster.classList.add("blue");
                monster.classList.remove("green");
            }
            if (life > 50)
                monster.classList.add("green");

            if (money <= 50)
                monster.classList.add("thin");

            if (money >= 100){
                monster.classList.add("medium");
                monster.classList.remove("thin");
            }
            if (money >= 150){
                monster.classList.add("thick");
                monster.classList.remove("medium");
            }

            document.getElementById('status').innerHTML = `
            <li>life: ${life}</li>
            <li>money: ${money}</li>
            <li>awake: ${awake}</li>`;
        }
    }
})();


window.addEventListener("load", monster.modules.app.start);